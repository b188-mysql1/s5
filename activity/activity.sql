-- Return the customerName of the customers who are from the Philippines
SELECT customerName FROM customers WHERE country = "Philippines";
-- Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName FROM customers WHERE customerName="La Rochelle Gifts";
-- Return the product name and MSRP of the product named "The Titanic";
SELECT productName, MSRP from products where productName = "The Titanic";
-- Return the first and last name of the employee whose email is "jfirrelli@classicmodelcars.com"
SELECT firstName, lastName from employees WHERE email = "jfirrelli@classicmodelcars.com";
-- Return the names of customers who have no registered state
SELECT customerName from customers WHERE state IS NULL;
-- Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
SELECT firstName, lastName, email from employees WHERE firstName = "Steve" AND lastName = "Patterson";
-- Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT customerName, country, creditLimit from customers WHERE creditLimit > 3000 AND country != "USA";
-- Return the customer numbers of orders whose comments contain the string 'DHL'
SELECT customerNumber from orders WHERE comments LIKE "%DHL%";
-- Return the product lines whose text description mentions the phrase 'state of the art'
SELECT productLine from productlines WHERE textDescription LIKE "%state of the art%";
-- Return the countries of customers without duplication
SELECT DISTINCT country from customers;
-- Return the statuses of orders without duplication
SELECT DISTINCT status from orders;
-- Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT customerName, country from customers WHERE country = "USA" OR country = "France" OR country = "Canada";
-- Return the first name, last name, and office's city of employees whose offices are in Tokyo
SELECT employees.firstName, employees.lastName, offices.city from offices 
	JOIN employees ON offices.officeCode = employees.officeCode
	WHERE city = "Tokyo";
-- Return the customer names of customers who were served by the employee named "Leslie Thompson"
SELECT customers.customerName from customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	WHERE lastName = "Thompson" AND firstName = "Leslie";
-- Return the product names and customer name of products ordered by "Baane Mini Imports"
SELECT customers.customerName, products.productName from products 
	JOIN orderdetails ON products.productCode = orderdetails.productCode
	JOIN orders ON orderdetails.orderNumber = orders.orderNumber
	JOIN customers ON orders.customerNumber = customers.customerNumber
	WHERE customers.customerName = "Baane Mini Imports";
-- Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country from customers
	JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
	JOIN offices ON employees.officeCode = offices.officeCode
	WHERE offices.country = customers.country;
-- Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
SELECT productName, quantityInStock from products WHERE productLine = "Planes" AND quantityInStock < 1000;
-- Return the customer's names with a phone number containing "+81".
SELECT customerName from customers WHERE phone LIKE "%+81%";